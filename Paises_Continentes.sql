CREATE DATABASE pruebas;

CREATE TABLE IF NOT EXISTS tierra(
    continentes VARCHAR(30) NOT NULL,
    paises VARCHAR(30) NOT NULL,
    nombre VARCHAR(30) NOT NULL
);

INSERT INTO tierra VALUE ("Europa","España","Jose Andres");
INSERT INTO tierra VALUE ("Asia","Rusia","Alba");
INSERT INTO tierra VALUE ("Africa","Zimbawe","Bogdan");
INSERT INTO tierra VALUE ("America","Cuba","Pablo");
INSERT INTO tierra VALUE ("Europa","Yugoslavia","Lydia");
INSERT INTO tierra VALUE ("Asia","Corea del Norte","Nerea");
INSERT INTO tierra VALUE ("Africa","Libia","Nuria");
INSERT INTO tierra VALUE ("Asia","China","Pablo");
INSERT INTO tierra VALUE ("Europa","Suiza","Andres");