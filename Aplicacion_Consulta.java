import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.sql.*;
import java.util.TimeZone;

public class Aplicacion_Consulta {
	public static void main(String[] args) {		
		JFrame mimarco=new Marco_Aplicacion();		
		mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		mimarco.setVisible(true);
	}
}

class Marco_Aplicacion extends JFrame{
	private String usuario = "root";
	private String contraseña = "p3@2018";
	private String url = "jdbc:mysql://localhost:3306/pruebas?serverTimezone=" + TimeZone.getDefault().getID();
	
	private Connection miConexion;
	private Statement miStatement;
	private JLabel textoPlano1, textoPlano2;
	private JComboBox continentes;	
	private JComboBox paises;	
	private JTextArea resultado;
	private PreparedStatement vista;
	private final String consultaContinente = "SELECT nombre FROM tierra WHERE paises=?";
	private final String consultaPais = "SELECT nombre FROM tierra WHERE continentes=?";
	private final String consultaEspecifica = "SELECT nombre FROM tierra WHERE continentes=? AND paises=?";
	private final String consultaTodo = "SELECT nombre FROM tierra";
	
	public Marco_Aplicacion(){	
		setTitle ("Consulta BBDD");	
		setBounds(500,300,400,400);		
		setLayout(new BorderLayout());
		
		JPanel menus=new JPanel();		
		menus.setLayout(new FlowLayout());
		
		textoPlano1 = new JLabel("Continentes: ");
		textoPlano2 = new JLabel("Paises: ");
		
		continentes=new JComboBox();		
		continentes.setEditable(false);	
		continentes.addItem("Todos");
		continentes.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent evt) {
            	String continenteSeleccionado = (String) continentes.getSelectedItem();
            	String listaPaises;
            	
            	if(continenteSeleccionado.equals("Todos")){
            		listaPaises = "SELECT paises FROM tierra";
            	}else{
            		listaPaises = "SELECT paises FROM tierra WHERE continentes='"+continenteSeleccionado+"';";
            	}	
        		      		
        		ResultSet rs2;
				try{
					rs2 = miStatement.executeQuery(listaPaises);
					paises.removeAllItems();
					paises.addItem("Todos");
					while(rs2.next()){
	        			paises.addItem(rs2.getString("paises"));
	        		}
	        		rs2.close();
				}catch(SQLException e){
					e.printStackTrace();
				}      	
            }
        });
		
		paises=new JComboBox();		
		paises.setEditable(false);		
		paises.addItem("Todos");
		
		resultado= new JTextArea(4,50);		
		resultado.setEditable(false);		
		add(resultado);
		
		menus.add(textoPlano1);
		menus.add(continentes);	
		menus.add(textoPlano2);
		menus.add(paises);	
		
		add(menus, BorderLayout.NORTH);	
		add(resultado, BorderLayout.CENTER);
		
		JButton botonConsulta=new JButton("Consulta");		
		botonConsulta.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				ejecutaConsulta();		
			}		
		});
		add(botonConsulta, BorderLayout.SOUTH);
		
		//------CONEXION CON BBDD-------	
		try{
			miConexion = DriverManager.getConnection(url, usuario, contraseña);		
			miStatement = miConexion.createStatement();	
			String listaContinentes = "SELECT DISTINCTROW continentes FROM tierra";
			ResultSet rs = miStatement.executeQuery(listaContinentes);
			
			while(rs.next()){
				continentes.addItem(rs.getString("continentes"));
			}
			rs.close();
						
			String listaPaises = "SELECT DISTINCTROW paises FROM tierra";
			ResultSet rs2 = miStatement.executeQuery(listaPaises);
			
			while(rs2.next()){
				paises.addItem(rs2.getString("paises"));
			}
			rs2.close();						
		}catch(Exception e){
			System.out.println("Toca rayarse para encontrar el error");
			System.out.println(e);
		}					
	}
	
	private void ejecutaConsulta(){
		ResultSet rs = null;
		
		try{
			resultado.setText("");
			
			String paisSeleccionado = (String) paises.getSelectedItem();
			String continenteSeleccionado = (String) continentes.getSelectedItem();
			
			if(paisSeleccionado.equals("Todos") && continenteSeleccionado.equals("Todos")){
				vista = miConexion.prepareStatement(consultaTodo);
				rs = vista.executeQuery();
			}else if(paisSeleccionado.equals("Todos") && !continenteSeleccionado.equals("Todos")){
				vista = miConexion.prepareStatement(consultaPais);
				vista.setString(1, continenteSeleccionado);
				rs = vista.executeQuery();
			}else{
				vista = miConexion.prepareStatement(consultaEspecifica);
				vista.setString(1, continenteSeleccionado);
				vista.setString(2, paisSeleccionado);
				rs = vista.executeQuery();
			}
			
			while(rs.next()){
				resultado.append(rs.getString("nombre"));
				resultado.append("\n");
			}
		}catch(Exception e){
			System.out.println("Toca rayarse para encontrar el error");
			e.printStackTrace();
		}
	}
}